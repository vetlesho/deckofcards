package edu.ntnu.idatt2003.deckofcards.view;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;

public class CardPane extends Pane {
  public CardPane(String card) {
    super();
    createCardPane(card);
  }

  private void createCardPane(String card) {
    Label cardLabel = new Label(card);
    cardLabel.setStyle("-fx-font-size: 32px; -fx-text-fill: white;");
    Rectangle cardShape = getRectangle(card);

    getChildren().addAll(cardShape, cardLabel);
    cardLabel.layoutXProperty().bind(cardShape.widthProperty().subtract(cardLabel.widthProperty()).divide(2));
    cardLabel.layoutYProperty().bind(cardShape.heightProperty().subtract(cardLabel.heightProperty()).divide(2));
  }

  private static Rectangle getRectangle(String card) {
    Rectangle cardShape = new Rectangle(100, 150); // Adjust dimensions as needed
    if (card.contains("H") || card.contains("D")) {
      cardShape.setStyle("-fx-fill: #C60C30; -fx-stroke: black; -fx-stroke-width: 1px; -fx-arc-height: 20; -fx-arc-width: 20;");
    } else if (card.contains("S") || card.contains("C")) {
      cardShape.setStyle("-fx-fill: black; -fx-stroke: black; -fx-stroke-width: 1px; -fx-arc-height: 20; -fx-arc-width: 20;");
    } else {
      cardShape.setStyle("-fx-fill: grey; -fx-stroke: black; -fx-stroke-width: 1px; -fx-arc-height: 20; -fx-arc-width: 20;");
    }
    return cardShape;
  }

}
