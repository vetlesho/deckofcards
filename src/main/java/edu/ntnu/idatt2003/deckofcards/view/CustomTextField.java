package edu.ntnu.idatt2003.deckofcards.view;

import javafx.scene.control.TextField;

public class CustomTextField extends TextField {
    public CustomTextField(){
        super();
        setStyle("-fx-background-color: #333333; -fx-text-fill: white;");
        setPrefWidth(50);
    }
}
