package edu.ntnu.idatt2003.deckofcards.view;

import javafx.geometry.Insets;
import javafx.scene.control.Label;

public class CustomLabel extends Label {
  public CustomLabel(String text) {
    super(text);
    setPadding(new Insets(0,0,0,20));
    setStyle("-fx-font-size: 20px; -fx-text-fill: white;");
  }
}

