package edu.ntnu.idatt2003.deckofcards.view;

import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class CustumButton extends Button{
    public CustumButton(String text){
      super(text);
      setStyle("-fx-background-color: linear-gradient(#2f6b4d, #3a604d);"
              + "-fx-background-radius: 30;"
              + "-fx-text-fill: white;");
      setFont(Font.font("Arial", FontWeight.BOLD, 16));
      setPrefWidth(130);
      setPrefHeight(40);
      setEffect(new DropShadow());
    }
}
