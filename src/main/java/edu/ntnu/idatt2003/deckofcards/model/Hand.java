package edu.ntnu.idatt2003.deckofcards.model;

import java.util.ArrayList;

public class Hand {
  private final ArrayList<String> hand;

  public Hand(int size) {
    hand = new ArrayList<>(size);
  }

  public ArrayList<String> getHand() {
    return hand;
  }

  public void add(String card) {
    hand.add(card);
  }


  public void clearHand() {
    hand.clear();
  }
  @Override
    public String toString() {
        return hand + " ";
    }
}
