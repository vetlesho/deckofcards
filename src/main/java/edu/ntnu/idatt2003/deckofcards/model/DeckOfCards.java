package edu.ntnu.idatt2003.deckofcards.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class DeckOfCards {
  private final char[] suit = {'S', 'H', 'D', 'C'};
  private final String[] rank = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
  private final ArrayList<String> deck;

  public DeckOfCards() {
    deck = new ArrayList<>();
    initializeDeck();
  }

  public char[] getSuit() {
    return suit;
  }

  public String[] getRank() {
    return rank;
  }

  public ArrayList<String> getDeck() {
    return deck;
  }

  //Initializes the deck with all 52 cards
  private void initializeDeck() {
    for (char suit : suit) {
      for (String rank : rank) {
        deck.add(suit + rank);
      }
    }
  }

  public void shuffle() {
    Random random = new Random();
    for (int i = 0; i < deck.size(); i++) {
      int randomIndex = random.nextInt(deck.size());
      String temp = deck.get(i);
      deck.set(i, deck.get(randomIndex));
      deck.set(randomIndex, temp);
    }
  }

  public void dealHand(int numberOfCards, Hand hand) {
    Random random = new Random();
    for (int i = 0; i < numberOfCards; i++) {
      //Find a random card in the deck
      int cardIndex = random.nextInt(deck.size());

      //Add the card to the hand
      hand.add(deck.get(cardIndex));

      deck.remove(cardIndex);
    }
  }

  public void collectHand(Hand hand) {
    deck.addAll(hand.getHand());
    hand.clearHand();
  }

  public boolean checkFlush(Hand hand, Hand dealer) {
    ArrayList<String> allCards = new ArrayList<>();
    allCards.addAll(hand.getHand());
    allCards.addAll(dealer.getHand());

    for (String suit : Arrays.asList("S", "H", "D", "C")) {
      long count = allCards.stream().filter(card -> card.startsWith(suit)).count();
      if (count >= 5) {
        return true;
      }
    }
    return false;
  }

  public int checkPair(Hand hand, Hand dealer) {
    ArrayList<String> allCards = new ArrayList<>();
    allCards.addAll(hand.getHand());
    allCards.addAll(dealer.getHand());

    int countPair = 0;
    for (String rank : rank) {
      long count = allCards.stream().filter(card -> card.endsWith(rank)).count();
      if (count >= 2) {
        countPair++;
      }
    }
    return countPair;
  }

  public boolean checkForSpadeKing(Hand hand) {
    return hand.getHand().contains("SK");
  }

  @Override
  public String toString() {
    return deck + " ";
  }
}
