package edu.ntnu.idatt2003.deckofcards;

import edu.ntnu.idatt2003.deckofcards.view.CustomLabel;
import edu.ntnu.idatt2003.deckofcards.view.CustomTextField;
import edu.ntnu.idatt2003.deckofcards.model.DeckOfCards;
import edu.ntnu.idatt2003.deckofcards.model.Hand;
import edu.ntnu.idatt2003.deckofcards.view.CardPane;
import edu.ntnu.idatt2003.deckofcards.view.CustumButton;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import java.io.IOException;

public class Main extends Application {
  private DeckOfCards deck;
  private Hand player1;
  private Hand dealer;
  private HBox playerHand;
  private HBox dealerHand;
  private CustomTextField flushTextField;
  private CustomTextField pairTextField;
  private CustomTextField kingOfSpadeTextField;
  private final int PLAYER_CARDS = 5;
  private final int DEALER_CARDS = 2;

  public static void main(String[] args) {
    launch();
  }

  private void initialize() {
    deck = new DeckOfCards();
    player1 = new Hand(PLAYER_CARDS);
    dealer = new Hand(DEALER_CARDS);
  }

  @Override
  public void start(Stage primaryStage) throws IOException {
    initialize();

    BorderPane startScreen = new BorderPane();
    startScreen.setStyle("-fx-background-color: #4d4d4d;");
    VBox startContent = new VBox();
    startContent.setAlignment(Pos.CENTER);

    Label welcomeLabel = new CustomLabel("""
            Welcome to Poker with dealer!\s
            In this game you will be dealt 5 cards, and the dealer will be dealt 2 cards.\s
            You can then check if you have a flush, pairs or the king of spade.\s
            Good luck!"""
    );
    welcomeLabel.setPadding(new Insets(0, 0, 40, 0));

    Button startButton = new CustumButton("Start game");
    startButton.setOnAction(e -> primaryStage.setScene(createGameScene()));

    startContent.getChildren().addAll(welcomeLabel, startButton);
    startScreen.setCenter(startContent);

    Scene scene = new Scene(startScreen, 800, 550);
    primaryStage.setScene(scene);
    primaryStage.setTitle("Poker with dealer");
    primaryStage.show();
  }

  private Scene createGameScene(){
    BorderPane root = new BorderPane(); //main layout
    VBox sideMenu = new VBox(); //menu on the right
    VBox playingBoard = new VBox(); //content in the center
    BorderPane bottomBar = new BorderPane(); //bottom bar
    HBox bottomContent = new HBox(); //content in bottomBar

    //Set background color for the panes
    root.setStyle("-fx-background-color: #4d4d4d;"); // Set background color
    playingBoard.setStyle("-fx-background-color: #2f6b4d;"); // Set background color for the playing board

    //Set padding, air around the content of the panes
    sideMenu.setPadding(new Insets(0, 30, 0, 30));
    bottomBar.setPadding(new Insets(30, 10, 30, 10));

    //Playing board
    dealerHand = new HBox(3);
    dealerHand.setAlignment(Pos.TOP_CENTER);
    playerHand = new HBox(5);
    playerHand.setAlignment(Pos.BOTTOM_CENTER);
    playerHand.setPadding(new Insets(20, 0, 0, 0));

    playingBoard.getChildren().addAll(dealerHand, playerHand);
    playingBoard.setAlignment(Pos.CENTER);

    //Side menu content
    Button dealButton = new CustumButton("Deal cards");
    dealButton.setOnAction(e -> {
      clearTable();
      deck.dealHand(PLAYER_CARDS, player1);
      deck.dealHand(DEALER_CARDS, dealer);
      showPlayerHand();
      hideDealerHand();
    });

    Button checkCards = new CustumButton("Check cards");
    checkCards.setOnAction(e -> {
      showDealerHand();
      checkForMatch();
    });

    sideMenu.getChildren().addAll(dealButton, checkCards);
    sideMenu.setSpacing(30);
    sideMenu.setAlignment(Pos.TOP_CENTER);

    //Bottom bar
    Label flushLabel = new CustomLabel("Flush:");
    flushTextField = new CustomTextField();
    Label pairLabel = new CustomLabel("Pairs:");
    pairTextField = new CustomTextField();
    Label kingOfSpade = new CustomLabel("King of Spade");
    kingOfSpadeTextField = new CustomTextField();
    bottomContent.getChildren().addAll(flushLabel, flushTextField, pairLabel, pairTextField, kingOfSpade, kingOfSpadeTextField);

    bottomContent.setAlignment(Pos.CENTER);
    bottomBar.setLeft(bottomContent);
    bottomBar.setRight(sideMenu);
    bottomBar.setPrefHeight(80);

    //set the panes in the root layout
    root.setCenter(playingBoard);
    root.setBottom(bottomBar);

    return new Scene(root, 800, 550);
  }

  private void showPlayerHand() {
    playerHand.getChildren().clear();
    for (String card : player1.getHand()) {
      CardPane cardPane = new CardPane(card);
      playerHand.getChildren().add(cardPane);
    }
  }

  private void hideDealerHand() {
    for (int i = 0; i < DEALER_CARDS; i++) {
      CardPane cardShape = new CardPane(" ");
      dealerHand.getChildren().add(cardShape);
    }
  }

  private void showDealerHand() {
    dealerHand.getChildren().clear();
    for (String card : dealer.getHand()) {
      CardPane cardPane = new CardPane(card);
      dealerHand.getChildren().add(cardPane);
    }
  }

  private void checkForMatch() {
    if (player1.getHand().isEmpty()) {
      showAlert();
    }
    checkForFlush();
    checkForPair();
    checkForKingOfSpade();
  }

  private void showAlert() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle("Error");
    alert.setHeaderText(null);
    alert.setContentText("You need to deal cards first!");
    alert.showAndWait();
  }

  private void checkForFlush() {
    if (deck.checkFlush(player1, dealer)) {
      flushTextField.setText("Yes!");
    } else {
      flushTextField.setText("No!");
    }
  }

  private void checkForPair() {
    String number = String.valueOf(deck.checkPair(player1, dealer));
    pairTextField.setText(number);
  }

  private void checkForKingOfSpade() {
    if (deck.checkForSpadeKing(player1)) {
      kingOfSpadeTextField.setText("Yes!");
    } else {
      kingOfSpadeTextField.setText("No!");
    }
  }

  private void clearTable() {
    deck.collectHand(player1);
    deck.collectHand(dealer);
    deck.shuffle();
    playerHand.getChildren().clear();
    dealerHand.getChildren().clear();
    flushTextField.clear();
    pairTextField.clear();
    kingOfSpadeTextField.clear();
  }
}