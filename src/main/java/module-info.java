module edu.ntnu.idatt2003.deckofcards {
  requires javafx.controls;
  requires javafx.fxml;


  opens edu.ntnu.idatt2003.deckofcards to javafx.fxml;
  exports edu.ntnu.idatt2003.deckofcards;
  exports edu.ntnu.idatt2003.deckofcards.model;
  opens edu.ntnu.idatt2003.deckofcards.model to javafx.fxml;
  exports edu.ntnu.idatt2003.deckofcards.view;
  opens edu.ntnu.idatt2003.deckofcards.view to javafx.fxml;
}