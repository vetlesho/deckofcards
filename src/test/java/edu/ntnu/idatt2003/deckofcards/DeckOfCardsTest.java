package edu.ntnu.idatt2003.deckofcards;

import edu.ntnu.idatt2003.deckofcards.model.DeckOfCards;
import edu.ntnu.idatt2003.deckofcards.model.Hand;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {
  private DeckOfCards deck;
  @BeforeEach
  void setUp() {
    deck = new DeckOfCards();
  }

  @AfterEach
  void tearDown() {
    deck = null;
  }

  @Test
  void initializeDeck() {
    assertEquals(52, deck.getDeck().size());
  }

  @Test
  void shuffle() {
    DeckOfCards deck2 = new DeckOfCards();
    deck2.shuffle();
    assertNotEquals(deck.getDeck(), deck2.getDeck());
  }

  @Test
  void dealHand() {
    Hand hand = new Hand(5);
    deck.dealHand(5, hand);
    assertEquals(47, deck.getDeck().size());
    assertEquals(5, hand.getHand().size());
  }

  @Test
  void collectHand() {
    Hand hand = new Hand(5);
    deck.dealHand(5, hand);
    assertEquals(47, deck.getDeck().size());
    deck.collectHand(hand);
    assertEquals(52, deck.getDeck().size());
    assertEquals(0, hand.getHand().size());
  }


  @Nested
  class CheckFlush {
    @Test
    void checkFlushWithDealer() {
      Hand hand = new Hand(5);
      hand.add("S2");
      hand.add("S3");
      hand.add("S4");
      hand.add("S5");
      hand.add("H6");

      Hand dealer = new Hand(2);
      dealer.add("C2");
      dealer.add("S3");
      assertTrue(deck.checkFlush(hand, dealer));
    }

    @Test
    void checkFlushAlone() {
      Hand hand = new Hand(5);
      hand.add("S2");
      hand.add("S3");
      hand.add("S4");
      hand.add("S5");
      hand.add("S6");

      Hand dealer = new Hand(2);
      dealer.add("C2");
      dealer.add("C3");
      assertTrue(deck.checkFlush(hand, dealer));
    }

    @Test
    void checkFlushFalse() {
      Hand hand = new Hand(5);
      hand.add("S2");
      hand.add("S3");
      hand.add("S4");
      hand.add("S5");
      hand.add("H6");

      Hand dealer = new Hand(2);
      dealer.add("C2");
      dealer.add("H3");
      assertFalse(deck.checkFlush(hand, dealer));
    }
  }

  @Nested
  class testPair {
    @Test
    void test1PairWithDealer() {
      Hand hand = new Hand(5);
      hand.add("S2");
      hand.add("S3");
      hand.add("S4");
      hand.add("S5");
      hand.add("H6");

      Hand dealer = new Hand(2);
      dealer.add("C2");
      dealer.add("S9");
      assertEquals(1, deck.checkPair(hand, dealer));
    }

    @Test
    void test2PairWithDealer() {
      Hand hand = new Hand(5);
      hand.add("S2");
      hand.add("S3");
      hand.add("S4");
      hand.add("S5");
      hand.add("H6");

      Hand dealer = new Hand(2);
      dealer.add("C2");
      dealer.add("S3");
      assertEquals(2, deck.checkPair(hand, dealer));
    }

    @Test
    void test3PairWithDealer() {
      Hand hand = new Hand(5);
      hand.add("S2");
      hand.add("S3");
      hand.add("S4");
      hand.add("S5");
      hand.add("H4");

      Hand dealer = new Hand(2);
      dealer.add("C2");
      dealer.add("S3");
      assertEquals(3, deck.checkPair(hand, dealer));
    }

    @Test
    void testNoPairWithDealer() {
      Hand hand = new Hand(5);
      hand.add("S2");
      hand.add("S3");
      hand.add("S4");
      hand.add("S5");
      hand.add("H6");

      Hand dealer = new Hand(2);
      dealer.add("C7");
      dealer.add("S8");
      assertEquals(0, deck.checkPair(hand, dealer));
    }

    @Test
    void test1PairAlone() {
      Hand hand = new Hand(5);
      hand.add("S2");
      hand.add("S3");
      hand.add("S4");
      hand.add("S5");
      hand.add("H4");

      Hand dealer = new Hand(2);
      dealer.add("C7");
      dealer.add("S8");
      assertEquals(1, deck.checkPair(hand, dealer));
    }
  }

  @Nested
  class testKingSpade {
    @Test
    void testKingSpadeFalse() {
      Hand hand = new Hand(5);
      hand.add("S2");
      hand.add("S3");
      hand.add("S4");
      hand.add("S5");
      hand.add("H6");
      assertFalse(deck.checkForSpadeKing(hand));
    }

    @Test
    void testKingSpadeTrue() {
      Hand hand = new Hand(5);
      hand.add("S2");
      hand.add("S3");
      hand.add("S4");
      hand.add("S5");
      hand.add("SK");
      assertTrue(deck.checkForSpadeKing(hand));
    }
  }



}