package edu.ntnu.idatt2003.deckofcards;

import edu.ntnu.idatt2003.deckofcards.model.Hand;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HandTest {
  private Hand hand;
  @BeforeEach
  void setUp() {
    hand = new Hand(5);
  }
  @AfterEach
  void tearDown() {
    hand = null;
  }

  @Test
  void getAddAndGetHand() {
    hand.add("S2");
    hand.add("H3");
    hand.add("D4");
    hand.add("C5");
    hand.add("S6");
    assertEquals(5, hand.getHand().size());
  }

  @Test
  void clearHand() {
    hand.clearHand();
    assertEquals(0, hand.getHand().size());
  }
}